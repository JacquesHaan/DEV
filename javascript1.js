const circle = document.querySelector('#flyObject');
const container = document.querySelector('#flyContainer');

container.addEventListener('click', getClickPosition, false);

let bgMusic = new Audio("song.mp3")

let musicContr = document.querySelector("#volume");

let speedContr = document.querySelector("#speed");

let colorWell;
let defColor = "#9900CC";

window.addEventListener("load", startup, false);

function startup() {
  colorWell = document.querySelector("#colorWell");
  colorWell.value = defColor;
  colorWell.addEventListener("input", updateFirst, false);
  colorWell.addEventListener("change", updateAll, false);
  colorWell.select();
}

musicContr.addEventListener("change", function(e) {
  bgMusic.volume = e.currentTarget.value / 100;
})



function getClickPosition(e) {
  const parentPos = getPosition(container);
  const xPos = e.clientX - parentPos.x - (circle.offsetWidth / 2);
  const yPos = e.clientY - parentPos.y - (circle.offsetHeight / 2);
  
  const translate3dValue = 'translate3d('+ xPos + 'px,' + yPos +  'px, 0)';
  circle.style.transform = translate3dValue;

  bgMusic.play();
}

function getPosition(element) {
  let xPos = 0;
  let yPos = 0;
  
  while(element) {
    xPos += (element.offsetLeft - element.scrollLeft + element.clientLeft);
    yPos += (element.offsetTop - element.scrollTop + element.clientTop);
    
    element = element.offsetParent;
  }
  
  return {
    x: xPos,
    y: yPos
  }
}