    let up = false,
    right = false,
    down = false,
    left = false,

    x = window.innerWidth/2-130/2,
    y = window.innerHeight/2-130/2

    document.addEventListener('keydown',press)
    function press(e){
        if (e.keyCode === 38 /* up */ || e.keyCode === 87 /* w */ || e.keyCode === 90 /* z */){
            up = true
        }
        if (e.keyCode === 39 /* right */ || e.keyCode === 68 /* d */){
            right = true
        }
        if (e.keyCode === 40 /* down */ || e.keyCode === 83 /* s */){
            down = true
        }
        if (e.keyCode === 37 /* left */ || e.keyCode === 65 /* a */ || e.keyCode === 81 /* q */){
            left = true
        }
        bgMusic.play();
    }

document.addEventListener('keyup',release)
    function release(e){
        if (e.keyCode === 38 /* up */ || e.keyCode === 87 /* w */ || e.keyCode === 90 /* z */){
            up = false
        }
        if (e.keyCode === 39 /* right */ || e.keyCode === 68 /* d */){
            right = false
        }
        if (e.keyCode === 40 /* down */ || e.keyCode === 83 /* s */){
            down = false
        }
        if (e.keyCode === 37 /* left */ || e.keyCode === 65 /* a */ || e.keyCode === 81 /* q */){
            left = false
        }
    }

    let Speed = 4;

    let speedContr = document.querySelector("#speedChange");

    speedContr.addEventListener("change", function(e) {
        Speed = e.currentTarget.value / 5;
      })

    function gameLoop(){
        var div = document.querySelector('div')
            if (up){
                y = y - Speed
            }
            if (right){
                x = x + Speed
            }
            if (down){
                y = y + Speed
            }
            if (left){
                x = x - Speed
            }

            div.style.left = x+'px'
            div.style.top = y+'px'
    window.requestAnimationFrame(gameLoop)
        }
    window.requestAnimationFrame(gameLoop)

    let bgMusic = new Audio("song.mp3")

    let musicContr = document.querySelector("#volChange");

    musicContr.addEventListener("change", function(e) {
        bgMusic.volume = e.currentTarget.value / 100;
      })